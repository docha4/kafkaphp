<?php
/**
 * Created by PhpStorm.
 * User: usr
 * Date: 2018/09/24
 * Time: 12:21
 */

$rk = new RdKafka\Consumer();
$rk->setLogLevel(LOG_DEBUG);
$rk->addBrokers("localhost");

$topic = $rk->newTopic("mytest");

$topic->consumeStart(1, RD_KAFKA_OFFSET_BEGINNING);

while (true) {
    // The first argument is the partition (again).
    // The second argument is the timeout.
    $msg = $topic->consume(0, 1000);
    if ($msg->err) {
        echo $msg->errstr(), "\n";
        sleep(1);
    } else {
        echo $msg->payload, "\n";
    }
    usleep(500*1000);
}