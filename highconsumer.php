<?php
/**
 * Created by PhpStorm.
 * User: usr
 * Date: 2018/09/24
 * Time: 13:02
 */


$conf = new RdKafka\Conf();

// ログパーティション割り当てへのリバランス コールバックを設定 (任意)
$conf->setRebalanceCb(function (RdKafka\KafkaConsumer $kafka, $err, array $partitions = null) {
    switch ($err) {
        case RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS:
            echo "Assign: ";
            var_dump($partitions);
            $kafka->assign($partitions);
            break;

        case RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS:
            echo "Revoke: ";
            var_dump($partitions);
            $kafka->assign(NULL);
            break;

        default:
            throw new \Exception($err);
    }
});

// group.idを設定する。同じgroup.idを持つ全てのコンシューマが消費するでしょう
// 異なるパーティション。
$conf->set('group.id', 'myConsumerGroup');

// Kafkaブローカーの初期リスト
$conf->set('metadata.broker.list', 'localhost');

$topicConf = new RdKafka\TopicConf();

// オフセットストアに初期オフセットが無いか、あるいは望ましいオフセットが範囲外の時に、どこからメッセージの消費を開始するかを設定する

// 'smallest': 最初から開始する
//$topicConf->set('auto.offset.reset', 'smallest');

// 購読/割り当てオブジェクトのために使う構成を設定する
$conf->setDefaultTopicConf($topicConf);

$consumer = new RdKafka\KafkaConsumer($conf);

// トピック 'test' の購読
$consumer->subscribe(['mytest']);


echo "Waiting for partition assignment... (make take some time when\n";
echo "quickly re-joining the group after leaving it.)\n";

while (true) {
    $message = $consumer->consume(20 * 1000);
    switch ($message->err) {
        case RD_KAFKA_RESP_ERR_NO_ERROR:
            var_dump($message);
            $consumer->commit();
            break;
        case RD_KAFKA_RESP_ERR__PARTITION_EOF:
            echo "No more messages; will wait for more\n";
            break;
        case RD_KAFKA_RESP_ERR__TIMED_OUT:
            echo "Timed out\n";
            break;
        default:
            throw new \Exception($message->errstr(), $message->err);
            break;
    }


    usleep(10 * 1000);
}