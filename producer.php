<?php
/**
 * Created by PhpStorm.
 * User: usr
 * Date: 2018/09/24
 * Time: 12:10
 */

$rk = new RdKafka\Producer();
$rk->setLogLevel(LOG_DEBUG);
$rk->addBrokers("localhost");

$topic = $rk->newTopic("mytest");
$count = 0;
$t = time();
while (true) {
    $payload = "Message payload:" . date('Ymd-h:i:s', $t) . ":" . $count++;
    $topic->produce(($count % 2), 0, $payload);
    usleep(500 * 1000);
    echo $payload . "\r\n";
}
//docker run -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=`docker-machine ip \`docker-machine active\`` --env ADVERTISED_PORT=9092 spotify/kafka